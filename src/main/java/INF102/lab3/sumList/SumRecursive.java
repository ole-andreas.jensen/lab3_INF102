package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        if (list.size() == 0) {
            return 0;
        }
        long last_cipher = list.get(list.size() - 1);
        list.remove(list.size() - 1);
        return last_cipher + sum(list);
    }
}
