package INF102.lab3.numberGuesser;

public class MyGeniusGuesser implements IGuesser {

    private int upperBound;
    private int lowerBound;
    private int mid;

    @Override
    public int findNumber(RandomNumber number) {
        if (number == null)
            throw new IllegalArgumentException("The number is null");
        if (upperBound == mid || lowerBound == mid) {
            lowerBound = number.getLowerbound();
            upperBound = number.getUpperbound();
        }
        mid = (upperBound + lowerBound) / 2;
        int returnedGuess = number.guess(mid);

        if (returnedGuess == 0)
            return mid;
        if (returnedGuess == 1) {
            upperBound = mid - 1;
        }
        if (returnedGuess == -1) {
            lowerBound = mid + 1;
        }

        return findNumber(number);

    }
}
