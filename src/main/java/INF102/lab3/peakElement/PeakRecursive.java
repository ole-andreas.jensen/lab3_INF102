package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        if (numbers.isEmpty())
        throw new IllegalArgumentException("The list is empty");

        int x = numbers.size();
        if (x == 1)
            return numbers.get(0);

        if (numbers.get(0) >= numbers.get(1))
            return numbers.get(0);
        if (numbers.get(x - 1) >= numbers.get(x - 2))
            return numbers.get(x-1);
        

        numbers.remove(numbers.size() - 1);
        return peakElement(numbers);

    }

}
